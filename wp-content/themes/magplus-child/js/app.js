

jQuery(document).ready(function ($) {
  // TOGGLING FAVOURITE ICON	
	$(".fav-floater").click(function () {
	   $(this).find('path').toggleClass("yellow");
	});

	$(".fav-floater").hover(function ()	{
	    $(this).find('path').addClass("cls-1");
	},
	function (){
		$(this).find('path').removeClass("cls-1");
	});


$('.featured_posts_slider').slick({
	  dots: false,
	  infinite: false,
	  speed: 300,
	  arrows: true,
	  prevArrow:"<button type='button' class='slick-prev float-left'> <span class='fa-stack fa-lg'><i class='fa fa-circle fa-stack-2x'></i><i class='fa fa-chevron-left fa-stack-1x fa-inverse'></i></span></button>",
	  nextArrow:"<button type='button' class='slick-next float-right'><span class='fa-stack fa-lg'><i class='fa fa-circle fa-stack-2x'></i><i class='fa fa-chevron-right fa-stack-1x fa-inverse'></i></span></i></button>",
	  slidesToShow: 3,
	  slidesToScroll: 3,
	  adaptiveHeight:true,
	  responsive: [
	    {
	      breakpoint: 1024,
	      settings: {
	        slidesToShow: 3,
	        slidesToScroll: 3,
	        infinite: false,
	        dots:true,
	        arrows: false,
	      }
	    },
	    {
	      breakpoint: 800,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 2,
	        dots:true,
	        arrows: false,
	      }
	    },
	    {
	      breakpoint: 480,
	      settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1,
	        dots:true,
	        arrows: false,
	        infinite: false,
	      }
	    }
	    // You can unslick at a given breakpoint now by adding:
	    // settings: "unslick"
	    // instead of a settings object
	  ]
	});

});