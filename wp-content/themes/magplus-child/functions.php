<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

// BEGIN ENQUEUE PARENT ACTION
// AUTO GENERATED - Do not modify or remove comment markers above or below:

if ( !function_exists( 'chld_thm_cfg_parent_css' ) ):
    function chld_thm_cfg_parent_css() {
        wp_enqueue_style( 'chld_thm_cfg_parent', trailingslashit( get_template_directory_uri() ) . 'style.css', array( 'font-awesome-theme','ytv-playlist','bootstrap-theme','magplus-main-style' ) );
    }
endif;
add_action( 'wp_enqueue_scripts', 'chld_thm_cfg_parent_css', 10 );
         
if ( !function_exists( 'child_theme_configurator_css' ) ):
    function child_theme_configurator_css() {
        wp_enqueue_style( 'chld_thm_cfg_child', trailingslashit( get_stylesheet_directory_uri() ) . 'style.css', array( 'chld_thm_cfg_parent' ) );
    }
endif;
add_action( 'wp_enqueue_scripts', 'child_theme_configurator_css' );

function enqueue_custom_stylesheets() {
	wp_enqueue_style( 'mytheme-slick', get_template_directory_uri() . '/../magplus-child/css/mediaqueries.css' );
    wp_enqueue_style( 'mytheme-slick', get_template_directory_uri() . '/../magplus-child/css/slick.css' );
 	wp_enqueue_style( 'mytheme-slick-theme', get_template_directory_uri() . '/../magplus-child/css/slick-theme.css' );
}
add_action( 'wp_enqueue_scripts', 'enqueue_custom_stylesheets' );

function js_file() {
  wp_enqueue_script('app', trailingslashit( get_template_directory_uri() )."../magplus-child/js/app.js",array('jquery'), MAGPLUS_THEME_VERSION,true);
  wp_enqueue_script('slick', get_template_directory_uri()."/../magplus-child/js/slick.min.js", MAGPLUS_THEME_VERSION,true);
}
add_action( 'wp_enqueue_scripts', 'js_file' );

// END ENQUEUE PARENT ACTION


// adding the short code for VC custom elements start

function image_with_logo_func( $atts ) {
	$a = shortcode_atts( array(
			'extra_class' => '', 
			'id' => '',
		    'main_image_url' => 'main_img_url',
		    'logo_image_url' => 'logo_img_url'
		), $atts );
	return '<div class="wpb_wrapper">
	<div class="wpb_single_image wpb_content_element vc_align_left '.$a['extra_class'].'" id="'.$a['id'].'">
		
		<figure class="wpb_wrapper vc_figure">
			<div class="vc_single_image-wrapper   vc_box_border_grey"><img src="'.$a['main_image_url'].'" class="vc_single_image-img attachment-full" alt="">
				<img class="imglogo" src="'.$a['logo_image_url'].'" />
			</div>
		</figure>
	</div>
</div>';
}
add_shortcode( 'imglogotag', 'image_with_logo_func' );

// adding the short code for VC custom elements end

function featured_posts_func( $atts ) {
	$a = shortcode_atts( array(
			'extra_class' => '', 
			'id' => '',
		    'posts' => '1,439'
		), $atts );


$args = array(
    'post__in' => explode(",", $a['posts'])
);


$posts = get_posts($args);

$html = '<div class="container">
	<div class="row">';
$html .= '<div class="featured_posts_slider">'; 
foreach ($posts as $p){
	$html .= '<div class="w-24 card marg-b20">';
	$html .= '<img src="'.wp_get_attachment_url( get_post_thumbnail_id($p->ID), 'thumbnail' ).'" alt="book-"'.$p->ID.'" class="img-obj"> <div class="box">';
	$html .= '<h4 class="post-title"><b>'.$p->post_title.'</b></h4>';
	$html .= '<a href="'.get_permalink( $p->ID).'">READ MORE <i class="fa yellow fa-chevron-right"></i></a>';
	$html .= 	'</div> </div>';
}
$html .= "</div>
</div>"; 

return $html;
}

add_shortcode( 'featured_posts', 'featured_posts_func' );


function my_featured_products_func($atts) {
	$product= shortcode_atts( array(
			'extra_class' => 'extra_class',
			"as_child" => array('only' => 'aboutus_background_tag'), 
			'image_url' => 'image_url',
			'book_title'=>'book_title',
			'book_rate'=>'book_rate',
			'book_author'=>'book_author'
		), $atts );
	return '<div class="w-24 card no-padding surround">
						<div class=" fav-floater white ">
							<svg width="35px" height="32.5px" viewBox="0 0 35 32.5" enable-background="new 0 0 35 32.5" xml:space="preserve">
							<path class="white" stroke="#B4B4B4" stroke-miterlimit="10" d="M17.742,25.633l7.906-7.613c1.937-1.938,2.906-3.841,2.906-5.711
								c0-1.861-0.537-3.316-1.613-4.365c-1.073-1.049-2.559-1.574-4.453-1.574c-0.525,0-1.059,0.091-1.605,0.273
								C20.337,6.824,19.83,7.07,19.36,7.378c-0.47,0.31-0.874,0.6-1.212,0.87c-0.338,0.27-0.66,0.558-0.965,0.863
								c-0.305-0.305-0.625-0.593-0.965-0.863c-0.338-0.271-0.742-0.561-1.212-0.87c-0.469-0.308-0.977-0.554-1.521-0.736
								c-0.546-0.182-1.081-0.273-1.606-0.273c-1.895,0-3.38,0.525-4.454,1.574c-1.074,1.049-1.612,2.504-1.612,4.365
								c0,0.567,0.1,1.151,0.299,1.752c0.198,0.6,0.425,1.112,0.679,1.535c0.253,0.423,0.541,0.836,0.862,1.237
								c0.322,0.402,0.557,0.679,0.705,0.832c0.148,0.152,0.264,0.262,0.349,0.33l7.918,7.639c0.153,0.152,0.339,0.229,0.559,0.229
								C17.404,25.861,17.59,25.785,17.742,25.633z"/>
							</svg>
						</div>
						<img src="'.$product['image_url'].'" alt="book1" class="img-obj">
						<hr>
						<span class="initial">
							<div class="row no-margin">
								<div class="col-xs-9 ">
									<h4 class="book-title"><b>'.$product['book_title'].'</b></h4> 
									<p class="book-description">By '.$product['book_author'].'</p> 
								</div>
								<div class="col-xs-3 no-padding">
									<p class="rate"> <i class="fa fa-inr"></i> '.$product['book_rate'].'</p>
								</div> 
							</div>
						</span>
						<span class="onhover">
							<div class="row no-margin">
								<div class="col-md-6 no-padding" >
									<a href="#" class="how ">
										<div class="row "> 
											<div class="col-md-12 center-content">
												<svg class="hover_img" viewBox="0 0 118.49 103.49"><title>view</title><path d="M59.68,81.35c-10,0-20.77-4.62-31.15-13.4A87.56,87.56,0,0,1,15.79,54.64l-.85-1.13.85-1.13A88.63,88.63,0,0,1,28.53,39.07c10.38-8.77,21.14-13.4,31.15-13.4s20.76,4.63,31.14,13.4a83.55,83.55,0,0,1,12.74,13.31l.85,1.13-.85,1.13A87.56,87.56,0,0,1,90.82,68C80.44,76.73,69.68,81.35,59.68,81.35Zm-40-27.84c3.87,4.91,20.57,24.07,40,24.07s36.05-19.16,40-24.07c-4-4.91-20.57-24.06-40-24.06S23.62,48.6,19.66,53.51Z"/><path class="cls-1" d="M59.67,39A14.54,14.54,0,1,1,45.15,53.52,14.57,14.57,0,0,1,59.67,39Z"/><path d="M74.22,53.52A14.54,14.54,0,1,0,59.67,68,14.57,14.57,0,0,0,74.22,53.52ZM59.67,35.19A18.31,18.31,0,1,1,41.38,53.52,18.34,18.34,0,0,1,59.67,35.19Z"/></svg>
											</div>
											<div class="col-md-12 center-content">
												<h2 class="hover_text">VIEW BOOK</h2>
											</div>
										</div>

									</a>
								</div>
								<div class="col-md-6 no-padding ">
									<a href="#" class="howc">
										<div class="row "> 
											<div class="col-md-12 center-content">
												<svg class="hover_img" viewBox="0 0 126.67 88"><title>cart(grey)</title><path class="cls-2" d="M92.4,26H66.62v-13a1.59,1.59,0,1,0-3.17,0V26H38.24l-1.76-9.86a1.6,1.6,0,0,0-1.59-1.31H23.27a1.59,1.59,0,0,0,0,3.18H33.59l7.79,43.42a7.79,7.79,0,1,0,10,4.53,7.62,7.62,0,0,0-1-1.88H79.26a7.79,7.79,0,1,0,6.27-3.17h-41l-1.1-6.15H81a7.94,7.94,0,0,0,7.69-6L93.93,28a1.59,1.59,0,0,0-1.15-1.93,1.72,1.72,0,0,0-.39,0ZM48.75,68.76a4.61,4.61,0,1,1-4.61-4.61h0A4.61,4.61,0,0,1,48.75,68.76Zm41.4,0a4.61,4.61,0,1,1-4.61-4.61h0A4.6,4.6,0,0,1,90.15,68.76ZM85.61,48.05A4.75,4.75,0,0,1,81,51.65H42.9L38.79,29.2h24.6V40.39L57,34a1.58,1.58,0,0,0-2.24,2.24l9.12,9.06a1.59,1.59,0,0,0,.24.2l.12.06.16.08.16,0,.13,0a1.62,1.62,0,0,0,.62,0l.14,0,.16,0,.12-.1.12-.06a1.57,1.57,0,0,0,.25-.2l9-9a1.59,1.59,0,0,0-2.27-2.24l-6.35,6.35V29.2H90.38Z"/></svg>
											</div>
											<div class="col-md-12 center-content">
												<h2 class="hover_text">ADD TO CART</h2>
											</div>
										</div>  		
									</a>
								</div> 
							</div>
						</span>
					</div>';
}

add_shortcode( 'featured_products', 'my_featured_products_func' );






/**
 * Add your own footer widgets columns
 * Add a number between 1 to 7
 */
function my_footer_widgets_columns() {

	// Add your column number
	$columns = '5';

	// Return
	return $columns;

}
add_filter( 'ocean_footer_widgets_columns', 'my_footer_widgets_columns' );

/**
 * Create a new footer widget area
 */
function my_footer_widget_area() {

	// Footer 5
	register_sidebar( array(
		'name'			=> esc_html__( 'Footer 5', 'oceanwp' ),
		'id'            => 'footer-five',
        'name'          => esc_html__('Footer Sidebar '.$i, 'magplus'),
        'before_widget' => '<div id="%1$s" class="widget tt-footer-list footer_widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h5 class="tt-title-block-2">',
        'after_title'   => '</h5> <hr> <div class="empty-space marg-lg-b20"></div>',
        'description'   => 'Drag the widgets for sidebars.'
	) );
}
add_action( 'widgets_init', 'my_footer_widget_area', 11 );

