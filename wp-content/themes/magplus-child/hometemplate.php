<?php /* Template Name: HomeTemplate */ 

get_template_part('templates/temp_header'); ?>

	<?php while(have_posts()) : the_post(); ?>
		<?php the_content(); ?>
	<?php endwhile; ?>

<?php get_template_part('templates/temp_footer'); ?>