<?php 
//Adds thumbnail to post
add_theme_support('post-thumbnails'); 
//Adds Menus
add_theme_support('menus'); 
//Register right sidebar
register_sidebar(array(
  'name' => __( 'Right Hand Sidebar' ),
  'id' => 'right-sidebar',
  'description' => __( 'Widgets in this area will be shown on the right-hand side.' ),
  'before_title' => '<h2>',
  'after_title' => '</h2>',
  'before_widget' => '<div id="%1$s" class="widget %2$s">',
  'after_widget'  => '</div><!-- widget end -->'
));

function wpb_custom_new_menu() {
  register_nav_menu('my-custom-menu',__( 'My Custom Menu' ));
}
add_action( 'init', 'wpb_custom_new_menu' );

// wp_enqueue_script( 'twentyfifteen-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20141010', true );


remove_filter('the_content', 'wpautop');

add_action( 'wp_enqueue_scripts', 'YOURTHEMENAME_scripts' );

function YOURTHEMENAME_scripts() {
    wp_deregister_script('jquery-core');
    wp_register_script('jquery-core', '//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js', false, '3.2.1');
    wp_enqueue_script('jquery-core');
}

function js_file() {
  wp_enqueue_script('app', get_template_directory_uri()."/js/app.js",$in_footer = true);
  wp_enqueue_script('bootstrap', get_template_directory_uri()."/js/bootstrap.min.js",$in_footer = true);
}
add_action( 'wp_enqueue_scripts', 'js_file' );

function enqueue_custom_stylesheets() {
    wp_enqueue_style( 'mytheme-bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css' );
    wp_enqueue_style( 'mytheme-custom', get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'mytheme-mediaqueries', get_template_directory_uri() . '/css/mediaqueries.css' );
}
add_action( 'wp_enqueue_scripts', 'enqueue_custom_stylesheets' );


?>
