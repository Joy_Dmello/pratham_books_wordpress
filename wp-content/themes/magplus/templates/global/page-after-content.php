<?php
/**
 *
 * @package magplus
 */
$layout = (is_archive() || is_search()) ? 'right_sidebar':magplus_get_opt('main-layout');
if ($layout == 'right_sidebar'): ?>
  </div>
  <?php get_sidebar('right'); ?>
 </div><!-- .row -->
<?php elseif ($layout == 'left_sidebar'): ?>
  </div>
  <?php get_sidebar('left'); ?>
 </div><!-- .row -->
<?php elseif($layout == 'dual_sidebar'): ?>
  </div>
  <?php get_sidebar('left'); ?>
  <?php get_sidebar('right'); ?>
</div>
<?php else: ?>
	</div>
</div>
<?php endif; ?>
