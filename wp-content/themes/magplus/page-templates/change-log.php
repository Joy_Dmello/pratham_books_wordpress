<?php
/**
 * Template Name: Change Log
 *
 * @package magplus
*/
get_header(); ?>


<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="section-change-log">
        <div class="empty-space  marg-lg-b100 marg-sm-b50 marg-xs-b30"></div>

        <div class="tt-join-fb">Wanna join our Facebook group to request new features ? <a href="https://www.facebook.com/groups/magpluswp/" target="_blank">Go Here</a></div>
        <div class="empty-space  marg-lg-b50 marg-sm-b30 marg-xs-b20"></div>

        <h2 class="c-h3">Release Notes</h2>

        <h3 class="c-h5 no-border"># 1.4 <span>11 December 2017</span></h3>
        <ul>
          <li><span class="badge badge-new">New</span>Architecture Pro Demo</li>
          <li><span class="badge badge-new">New</span>Girly Pro Demo</li>
          <li><span class="badge badge-new">New</span>DIY Pro Demo</li>
          <li><span class="badge badge-new">New</span>Cinema Pro Demo</li>
          <li><span class="badge badge-new">New</span>Personal Pro Demo</li>
          <li><span class="badge badge-new">New</span>Featured Blog Shortcode (Style4 and Style5)</li>
          <li><span class="badge badge-added">Added</span>Polyang Support</li>
          <li><span class="badge badge-added">Added</span>Social Links now Open in New Tab</li>
          <li><span class="badge badge-added">Added</span>Typography Option for Main Menu and Sub Menu</li>
          <li><span class="badge badge-added">Added</span>Textual Logo with Typography Options</li>
          <li><span class="badge badge-fixed">Fixed</span>Minor Bugs</li>
          <li><span class="badge badge-update">Update</span>MagPlus Addon Plugin to Version 1.3</li>
        </ul>

        <h3 class="c-h5"># 1.3 <span>02 December 2017</span></h3>
        <ul>
          <li><span class="badge badge-new">New</span>Google AMP Supported</li>
          <li><span class="badge badge-new">New</span>New Demo Entertainment Pro</li>
          <li><span class="badge badge-fixed">Fixed</span>Social Follow Link Issues</li>
          <li><span class="badge badge-update">Update</span>MagPlus Addon Plugin to Version 1.2</li>
        </ul>

        <h3 class="c-h5"># 1.2 <span>28 November 2017</span></h3>
        <ul>

          <li><span class="badge badge-new">New</span>Added Ajax Load More</li>
          <li><span class="badge badge-new">New</span>Added Ajax Infinite Scroll</li>
          <li><span class="badge badge-added">Added</span>Author Page</li>
          <li><span class="badge badge-added">Added</span>Social Icon Option for Post Author</li>
          <li><span class="badge badge-added">Added</span>Color Option for Load More Button</li>
          <li><span class="badge badge-fixed">Fixed</span>TGM Yellow Pencil Slug</li>
          <li><span class="badge badge-fixed">Fixed</span>Mega Menu issue When no Post Category is Added</li>
          <li><span class="badge badge-fixed">Fixed</span>Mobile Sub Menu Issue</li>
          <li><span class="badge badge-improvements">Improvements</span>Parallax on Mobile</li>
          <li><span class="badge badge-update">Update</span>Documentation</li>
          <li><span class="badge badge-update">Update</span>MagPlus Addon Plugin to Version 1.1</li>
        </ul>

        <h3 class="c-h5"># 1.1 <span>22 November 2017</span></h3>
        <ul>
          <li><span class="badge badge-new">New</span>Added Parallax Option for Blog Single Featured Image</li>
          <li><span class="badge badge-new">New</span>Added Typography Option for Menu and Sub Menus</li>
          <li><span class="badge badge-fixed">Fixed</span>Minor Bugs</li>
        </ul>


        <div class="empty-space  marg-lg-b100 marg-sm-b50 marg-xs-b30"></div>
      </div>
    </div>
  </div>
</div>

<?php
get_footer();
