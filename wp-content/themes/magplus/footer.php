
<?php
/**
 * Footer file
 *
 * @package magplus
 * @since 1.0
 */
?>

<?php magplus_footer_template(magplus_get_opt('footer-template')); ?>
</div>
</div>
<?php wp_footer(); ?>
</body>
</html>
