<?php
/*
 * Post
*/
$sections[] = array(
  'icon' => 'el-icon-screen',
  'fields' => array(
    array(
      'id'    =>'post-enable-breaking-news',
      'type'  => 'switch',
      'title' => esc_html__('Is this Breaking News ? ', 'magplus'),
      'on'    => 'Yes',
      'off'   => 'No',
      'options' => array(
        '1' => 'On',
        '0' => 'Off',
      ),
      'default' => '0',
    ),
    array(
      'id'=>'post-enable-custom-overlay',
      'type' => 'switch',
      'title' => esc_html__('Enable Post Custom Overlay', 'magplus'),
      'subtitle'=> esc_html__('If on, custom overlay will be displayed.', 'magplus'),
      'options' => array(
        '1' => 'On',
        '0' => 'Off',
      ),
      'default' => '0',
    ),
    array(
      'id'        => 'post-overlay-first-color',
      'type'      => 'color',
      'title'     => esc_html__('First Color', 'magplus'),
      'default'   => '',
      'required'  => array('post-enable-custom-overlay', 'equals', array(1)),
    ),

    array(
      'id'        => 'post-overlay-second-color',
      'type'      => 'color',
      'title'     => esc_html__('Second Color', 'magplus'),
      'default'   => '',
      'required'  => array('post-enable-custom-overlay', 'equals', array(1)),
    ),
    array(
      'id'=>'post-style-local',
      'type' => 'select',
      'title' => esc_html__('Post Style', 'magplus'),
      'subtitle' => esc_html__('Select post style.', 'magplus'),
      'options' => array(
        'default'                    => esc_html__('Default','magplus'),
        'default-title-left-aligned' => esc_html__('Post Title Left','magplus'),
        'default-alt'                => esc_html__('No Hero','magplus'),
        'alternative'                => esc_html__('Big Hero','magplus'),
        'alternative-title-middle'   => esc_html__('Box Hero','magplus'),
        'alternative-big-one'        => esc_html__('Title Below Hero','magplus'),
        'alternative-cover'          => esc_html__('Hero Alternative','magplus'),
      ),
      'default' => '',
    ),
  )
);
