<?php
/*
 * Title Wrapper Section
*/
$sections[] = array(
  'title' => esc_html__('Title Wrapper', 'magplus'),
  'desc' => esc_html__('Change the title wrapper section configuration.', 'magplus'),
  'icon' => 'fa fa-window-restore',
  'fields' => array(

    array(
      'id'       => 'title-wrapper-enable-local',
      'type'     => 'button_set',
      'title'    => esc_html__('Enable Title Wrapper', 'magplus'),
      'subtitle' => esc_html__('If on, this layout part will be displayed.', 'magplus'),
      'options' => array(
        '1' => 'On',
        ''  => 'Default',
        '0' => 'Off',
      ),
      'default' => '',
    ),
    array(
      'id'       =>'page-header-local',
      'type'     => 'media',
      'url'      => true,
      'title'    => esc_html__('Background', 'magplus'),
      'subtitle' => esc_html__('Title wrapper background, color and other options.', 'magplus'),
    ),
    array(
      'id'        => 'title-wrapper-subheading-local',
      'type'      => 'text',
      'title'     => esc_html__('Sub Heading', 'magplus'),
      'default'   => '',
    ),
  ), // #fields
);
